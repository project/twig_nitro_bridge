<?php

namespace Drupal\twig_nitro_bridge\Error;

/**
 * Terrific file extension not defined error.
 *
 * @package Drupal\twig_nitro_bridge\Exception
 */
class TerrificFileExtensionNotDefinedError extends \Error {}
