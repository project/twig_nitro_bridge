<?php

namespace Drupal\twig_nitro_bridge\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Namics\Terrific\Config\ConfigReader;

/**
 * Reads configuration from the file config.json in the frontend directory.
 *
 * @package Drupal\twig_nitro_bridge\Services
 */
class FrontendConfigReader implements FrontendConfigReaderInterface {

  /**
   * The path to the frontend directory.
   *
   * @var string
   */
  private string $frontendPath;

  /**
   * The frontend configuration array.
   *
   * @var array
   */
  private array $frontendConfig;

  /**
   * FrontendConfigReader constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, FileSystemInterface $fileSystem) {
    $this->frontendPath = $fileSystem
      ->realpath(
        DRUPAL_ROOT . $configFactory->get('twig_nitro_bridge.settings')->get('frontend_dir')
      );

    $terrificConfigReader = new ConfigReader($this->frontendPath);
    $this->frontendConfig = $terrificConfigReader->getConfig();
  }

  /**
   * Returns the frontend configuration from the config.json file as an array.
   *
   * @return array
   *   The frontend configuration.
   */
  public function getConfig(): array {
    return $this->frontendConfig;
  }

  /**
   * Returns the path to the frontend directory.
   *
   * @return string
   *   Path.
   */
  public function getFrontendPath(): string {
    return $this->frontendPath;
  }

}
